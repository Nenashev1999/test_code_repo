#include <iostream>
 
using namespace std;
 
struct Time
{
    int hours = 0;
    int minutes = 0;
    int seconds = 0;
    Time() {}
    Time(int h, int m, int s) : hours(h), minutes(m), seconds(s) {}
    // copy ctor
};
 
 
class TimeOperation
{
public:
        Time current_time;
        TimeOperation() {}
        
        TimeOperation(int seconds) : current_time(seconds / 3600 % 24, seconds / 60 % 60, seconds % 60) {}
 
        TimeOperation(int hours_, int minutes_, int seconds_) : current_time(hours_, minutes_, seconds_) {}
 
        TimeOperation(const Time & t) : current_time(t) {}
 
        TimeOperation(const TimeOperation & t) : current_time(t.current_time) {}
 
        ~TimeOperation()
        {
        }
 
        TimeOperation & operator+=(const TimeOperation & rhs)
        {
            int t = rhs.current_time.seconds + rhs.current_time.minutes * 60 + rhs.current_time.hours * 60 * 60;
 
            int secs = current_time.seconds + t;
            current_time.seconds = secs % 60;
            
            current_time.minutes += (secs / 60) % 60;
            current_time.hours += (secs / (60 * 60) % 24);
            
            return *this;
        } 
 
        TimeOperation & operator++()
        {
            return *this += 1;
        }
 
        TimeOperation operator++(int)
        {
            TimeOperation temp = *this;
            *this += 1;
            return temp;
        }
 
        TimeOperation & operator=(const TimeOperation & t)
        {
            current_time.hours = t.current_time.hours;
            current_time.minutes = t.current_time.minutes;
            current_time.seconds = t.current_time.seconds;
            return *this;
        }
        void print() const
        {
            cout << "s = " << current_time.seconds << '\n';
            cout << "m = " << current_time.minutes << '\n';
            cout << "h = " << current_time.hours << '\n';
           
        }
};
 
TimeOperation operator+(const TimeOperation & lhs, const TimeOperation & rhs)
{
    TimeOperation sum = lhs;
    sum += rhs;
    return sum;
}
 
bool operator < (const TimeOperation & lhs, const TimeOperation & rhs)
{
    if (lhs.current_time.hours != rhs.current_time.hours)
        return lhs.current_time.hours < rhs.current_time.hours;
 
    if (lhs.current_time.minutes != rhs.current_time.minutes)
        return lhs.current_time.minutes < rhs.current_time.minutes;
 
    return lhs.current_time.seconds < rhs.current_time.seconds;
}
 
bool operator > (const TimeOperation & t_1, const TimeOperation & t_2)
{
    return t_2 < t_1;
}
 
bool operator == (const TimeOperation& t_1, const TimeOperation& t_2)
{
    return !(t_1 < t_2 || t_1 > t_2);
}
 
bool operator != (const TimeOperation& t_1, const TimeOperation& t_2)
{
    return !(t_1 == t_2);
}
 
int main()
{
    TimeOperation a;
}