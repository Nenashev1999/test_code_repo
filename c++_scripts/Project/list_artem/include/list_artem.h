//
// Created by artem on 01.07.22.
//

#ifndef TEST_VECTOR_LIST_ARTEM_H
#define TEST_VECTOR_LIST_ARTEM_H
#include <iostream>

template <class T>
struct Node
{
    T x;
    Node * prev = nullptr;
    Node * next = nullptr;
    Node(T x_, Node * prev_, Node * next_) : x(x_), prev(prev_), next(next_){}
    explicit Node(T x_) : x(x_){}
//    Node<T> * operator++(Node<T> * obj)
//    {
//        return obj->next;
//    }
};

template <class T, class Alloc=std::allocator<T>>
class list_artem{
public:
    list_artem()= default;
    explicit list_artem(const T & x);
    ~list_artem();
    void push_back(const T & x);
    void push_front(const T & x);
    void pop_back();
    void pop_front();
    void get_info();
    int size();
    bool empty();
    void insert(int pose, const T & x);
    T * begin();
    T * end();
private:
    size_t sz = 0;
    Node<T> * first = nullptr;
    Node<T> * last = nullptr;
};


template <class T, class Alloc>
list_artem<T, Alloc>::list_artem(const T & x)
{
    first = new Node<T>(x);
    last = first;
    ++sz;
}


template <class T, class Alloc>
list_artem<T, Alloc>::~list_artem()
{
    Node<T> * next = first;
    Node<T> * current = first;
    for (int i = 0; i < sz; ++i)
    {
        next = next->next;
        delete current;
        current = next;
    }
}


template <class T, class Alloc>
void list_artem<T, Alloc>::push_back(const T &x)
{
    if (sz > 0)
    {
        Node<T> * temp_ptr = new Node<T>(x);
        last->next = temp_ptr;
        temp_ptr->prev = last;
        last = temp_ptr;
        temp_ptr = nullptr;
        ++sz;

    }
    else
    {
        first = new Node<T>(x);
        last = first;
        ++sz;
    }
}


template <class T, class Alloc>
void list_artem<T, Alloc>::push_front(const T &x)
{
    if (sz > 0)
    {
        Node<T> * temp_ptr = new Node<T>(x);
        first->prev = temp_ptr;
        temp_ptr->next = first;
        first = temp_ptr;
        temp_ptr = nullptr;
        ++sz;

    }
    else
    {
        first = new Node<T>(x);
        last = first;
        ++sz;
    }
}


template <class T, class Alloc>
void list_artem<T, Alloc>::pop_back()
{
    if (sz > 1)
    {
        Node<T> * temp_ptr = last;
        last = last->prev;
        last->next = nullptr;
        delete temp_ptr;
        temp_ptr = nullptr;
        --sz;
    }
    else
    {
        std::cout << "Error list is 0-1 elemets!\n";
    }
}


template <class T, class Alloc>
void list_artem<T, Alloc>::pop_front()
{
    if (sz > 1)
    {
        Node<T> * temp_ptr = first;
        first = first->next;
        first->prev = nullptr;
        delete temp_ptr;
        temp_ptr = nullptr;
        --sz;
    }
    else
    {
        std::cout << "Error list is 0-1 elemets!\n";
    }
}


template <class T, class Alloc>
int list_artem<T, Alloc>::size()
{
    return sz;
}


template <class T, class Alloc>
bool list_artem<T, Alloc>::empty()
{
    if (sz > 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}


template <class T, class Alloc>
void list_artem<T, Alloc>::insert(int pose, const T & x)
{
    if (pose == 0)
    {
        this->push_front(x);
    }
    else if (pose == sz)
    {
        this->push_back(x);
    }
    else
    {
        Node<T> * ptr = first;
        for (int i = 0; i < pose; ++i)
        {
            ptr = ptr->next;
        }
        Node<T> * tmp_ptr = ptr->prev;
        Node<T> * new_node_ptr = new Node<T>(x);
        new_node_ptr->prev = tmp_ptr;
        new_node_ptr->next = ptr;
        ptr->prev = new_node_ptr;
        tmp_ptr->next = new_node_ptr;
        ptr = nullptr;
        new_node_ptr = nullptr;
        tmp_ptr = nullptr;
        ++sz;
    }
}


template <class T, class Alloc>
T * list_artem<T, Alloc>::begin()
{
    return first;
}


template <class T, class Alloc>
T * list_artem<T, Alloc>::end()
{
    return last;
}

template <class T, class Alloc>
void list_artem<T, Alloc>::get_info()
{
    std::cout << "----------------------------------------------\n";
    std::cout << "                     List\n";
    std::cout << "----------------------------------------------\n\n";
    std::cout << "size:\t" << sz << "\n";
    if (sz > 0)
    {
        Node<T> * ptr = first;
        for (int i = 0; i < sz; ++i)
        {
            std::cout << "Node:\t" << i << "\tvalue:\t" << ptr->x << "\n";
            if (ptr->next)
            {
                ptr = ptr->next;
            }
        }
    }
    else
    {
        std::cout << "List is empty\n";
    }

}

#endif //TEST_VECTOR_LIST_ARTEM_H
