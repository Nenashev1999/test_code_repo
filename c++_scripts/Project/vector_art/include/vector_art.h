#include <iostream>


template <class T, class Alloc=std::allocator<T>>
class vector_art
{
public:
    vector_art(int size_)
    {
        arr = std::allocator_traits<Alloc>::allocate(alloc, size_*2);
        cap = size_ * 2;
        sz = size_;
    }

    vector_art()
    {
        cap = 0;
        sz = 0;
    }

    ~vector_art()
    {
        for (int i = 0; i < sz; ++i)
        {
            std::allocator_traits<Alloc>::destroy(alloc, arr+i);
        }
        std::allocator_traits<Alloc>::deallocate(alloc, arr, cap);
    }
    void push_back(const T & x);
    void pop_back();

    T & operator[](size_t i)
    {
        return arr[i];
    }

    T & operator[](size_t i) const
    {
        return arr[i];
    }

    int size()
    {
        return sz;
    }

    void print_vector()
    {
        for (int i = 0; i < sz; ++i)
        {
            std::cout << arr[i] << " ";
        }
        std::cout << "\n";
    }

    // T & at(size_t i)
    // {
    //     if (i < 0 || i > sz - 1)
    //     {
    //         throw std::out_of_range;
    //     }
    //     else
    //     {
    //         return arr[i];
    //     }
    // }
private:
    unsigned int cap;
    unsigned int sz;
    typedef T tp;
    T* arr;
    Alloc alloc;
};

template <class T, class Alloc>
void vector_art<T, Alloc>::push_back(const T & x)
{
    if (sz < cap)
    {
        std::allocator_traits<Alloc>::construct(alloc, arr+sz, x);
        sz++;
        return;
    }
    T* newarr = std::allocator_traits<Alloc>::allocate(alloc, cap*2);
    for (int i = 0; i < cap; ++i)
    {
        std::allocator_traits<Alloc>::construct(alloc, newarr+i, arr[i]);
    }
    std::allocator_traits<Alloc>::construct(alloc, newarr+cap, x);
    ++sz;
    for (int i = 0; i < cap; ++i)
    {
        std::allocator_traits<Alloc>::destroy(alloc, arr+i);
    }
    std::allocator_traits<Alloc>::deallocate(alloc, arr, cap);
    arr = newarr;
    cap *= 2;
}

template <class T, class Alloc>
void vector_art<T, Alloc>::pop_back()
{
    std::allocator_traits<Alloc>::destroy(alloc, arr+sz-1);
    --sz;
}