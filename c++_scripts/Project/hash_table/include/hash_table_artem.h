#ifndef TEST_VECTOR_HASH_TABLE_ARTEM_H
#define TEST_VECTOR_HASH_TABLE_ARTEM_H
#include <iostream>
#include <vector>
#include <cmath>


template <class T=int, class M=int>
class hash_table_artem
{
public:
    hash_table_artem();
    ~hash_table_artem();
    void resize();
    void rehash();
    void remove_item_form_table(T key);
    void add_item_to_table(T key, M value);
    bool in_table(T key);
    M & get_data_with_key(T key);
    int get_table_id(int hash);
    void get_information_about_table();
private:
    struct Node
    {
        T key;
        M value;
        bool state;
        explicit Node(const T & key_, M & value_) : key(key_), state(true), value(value_) {}
    };
    int hash_function(T key);
    Node ** table_slots;
    constexpr static const double multiplier_1 = 4.35;
    constexpr static const double multiplier_2 = 56.231;
    const double rehash_coeficient = 0.75;
    int default_cap = 8;
    int cap = 0;
    int sz = 0;
    int sz_without_deleted = 0;
    bool debug_info = false;
};


template <class T, class M>
hash_table_artem<T, M>::hash_table_artem()
{
    cap = default_cap;
    table_slots = new Node * [default_cap];
    for (int i = 0; i < default_cap; ++i)
    {
        table_slots[i] = nullptr;
    }
}


template <class T, class M>
hash_table_artem<T, M>::~hash_table_artem()
{
    for (int i = 0; i < cap; ++i)
    {
        delete table_slots[i];
    }
    delete [] table_slots;
}


template <class T, class M>
void hash_table_artem<T, M>::resize()
{
    if (debug_info) std::cout << "RESIZE, prev size:\t"<< cap << "\tcurrent:\t" << cap * 2 << "\n";
    int prev_buffer = cap;
    cap *= 2;
    sz = 0;
    Node ** arr = new Node * [cap];
    for (int i = 0; i < cap; ++i)
    {
        arr[i] = nullptr;
    }
    std::swap(table_slots, arr);
    for (int i = 0; i < prev_buffer; ++i)
    {
        if (arr[i])
        {
            if (arr[i]->state)
            {
                add_item_to_table(arr[i]->key, arr[i]->value);
            }
        }
    }
    sz_without_deleted = sz;
    for (int i = 0; i < prev_buffer; ++i)
    {
        delete arr[i];
    }
    delete [] arr;
}


template <class T, class M>
void hash_table_artem<T, M>::rehash()
{
    if (debug_info) std::cout << "rehash\n";
    sz = 0;
    Node ** arr = new Node * [cap];
    for (int i = 0; i < cap; ++i)
    {
        arr[i] = nullptr;
    }
    std::swap(table_slots, arr);
    for (int i = 0; i < cap; ++i)
    {
        if (arr[i])
        {
            if (arr[i]->state)
            {
                add_item_to_table(arr[i]->key, arr[i]->value);
            }
        }
    }
    sz_without_deleted = sz;
    for (int i = 0; i < cap; ++i)
    {
        delete arr[i];
    }
    delete [] arr;
}


template <class T, class M>
int hash_table_artem<T, M>::hash_function(T key)
{
    return floor(multiplier_1 * (multiplier_2 * key));
}


template <class T, class M>
int hash_table_artem<T, M>::get_table_id(int hash)
{
    return hash % cap;
}


template<class T, class M>
M & hash_table_artem<T, M>::get_data_with_key(T key)
{
    int hash = get_table_id(hash_function(key));
    int i = 0;
    while (i < cap)
    {
        if (table_slots[hash])
        {
            if ((table_slots[hash]->key == key) && (table_slots[hash]->state))
            {
                return table_slots[hash] -> value;
            }
            else
            {
                ++hash;
            }
        }
        else
        {
            if (debug_info) std::cout << "No such element error\n";
            break;
        }
        ++i;
    }
    if (debug_info) std::cout << "Can not find such element\n";
}


template <class T, class M>
void hash_table_artem<T, M>::add_item_to_table(T key, M value)
{
    if (cap - sz < 3)
    {
        resize();
    }
    else if ((sz / cap) > rehash_coeficient)
    {
        rehash();
    }
    int hash = get_table_id(hash_function(key));
    int i = 0;
    while (i < cap)
    {
        if (!table_slots[hash])
        {
            break;
        }
        else if ((table_slots[hash]->key) == key && (table_slots[hash]->state))
        {
            table_slots[hash]->value = value;
            return;
        }
        else
        {
//            std::cout << "COLLISION\n";
            if (hash < cap)
            {
                ++hash;
            }
            else
            {
                hash = 0;
            }
            ++i;
        }
    }
    if (i == cap)
    {
        if (debug_info) std::cout << "Could not find place for item" << "\n";
        return;
    }
    else
    {
        table_slots[hash] = new Node(key, value);
        ++sz;
        ++sz_without_deleted;
        return;
    }
}


template <class T, class M>
void hash_table_artem<T, M>::remove_item_form_table(T key)
{
    int hash = get_table_id(hash_function(key));
    int i = 0;
    while (i < cap)
    {
        if (table_slots[hash])
        {
            if ((table_slots[hash]->key) == key && (table_slots[hash]->state))
            {
                table_slots[hash]->state = false;
                --sz_without_deleted;
                break;
            }
            else
            {
                ++hash;
            }
        }
        else
        {
            if (debug_info) std::cout << "No such element error\n";
            if (debug_info) std::cout << "Can not find such element\n";
            break;
        }
        ++i;
    }
    if ((sz / cap) > rehash_coeficient)
    {
        rehash();
    }
}


template <class T, class M>
bool hash_table_artem<T, M>::in_table(T key)
{
    int hash = get_table_id(hash_function(key));
    int i = 0;
    while (i < cap)
    {
        if (table_slots[hash])
        {
            if ((table_slots[hash]->key) == key && (table_slots[hash]->state))
            {
                return true;
            }
            else
            {
                ++hash;
            }
        }
        else
        {
//            std::cout << "No such element error\n";
            return false;
        }
        ++i;
    }
}


template <class T, class M>
void hash_table_artem<T, M>::get_information_about_table()
{
    std::cout << "---------------------------------------------------------------------" << "\n";
    std::cout << "                            Table information" << "\n";
    std::cout << "---------------------------------------------------------------------" << "\n";
    std::cout << "capacity: " << this->cap << "\t||\tsize:" << this->sz << "\t||\tsize without deleted: " << this->sz_without_deleted << "\n\n";
    for (int i = 0; i < this->cap; ++i)
    {
        std::cout << i << ": ";
        if (this->table_slots[i] == nullptr)
        {
            std::cout << "nullptr" << "   ---   empty slot\n";
        }
        else
        {
            if (this->table_slots[i]->state)
            {
                std::cout << "key:\t" << this->table_slots[i]->key << "\t||\tvalue: " << this->table_slots[i]->value << "\t||\tflag: " << this->table_slots[i]->state << "\t---object in table\n";
            }
            else
            {
                std::cout << "key:\t" << this->table_slots[i]->key << "\t||\tvalue: " << this->table_slots[i]->value << "\t||\tflag: " << this->table_slots[i]->state << "\t---removed object in table\n";
            }

        }
    }
}
#endif //TEST_VECTOR_HASH_TABLE_ARTEM_H
