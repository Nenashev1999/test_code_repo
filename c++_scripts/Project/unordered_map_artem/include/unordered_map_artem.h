#ifndef TEST_VECTOR_UNORDERED_MAP_ARTEM_H
#define TEST_VECTOR_UNORDERED_MAP_ARTEM_H

#include <iostream>
#include <hash_table_artem.h>

template <class T, class M>
class unordered_map_artem
{
public:
    M & operator[](T key)
    {
        if (!table.in_table(key))
        {
            table.add_item_to_table(key, 0);
        }
        return table.get_data_with_key(key);
    }

    void get_data()
    {
        table.get_information_about_table();
    }

    void insert(T key)
    {
        table.remove_item_form_table(key);
    }
private:
    hash_table_artem<T, M> table;
};



#endif //TEST_VECTOR_UNORDERED_MAP_ARTEM_H
