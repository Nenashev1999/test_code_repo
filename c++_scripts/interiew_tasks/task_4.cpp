#include <iostream>
#include <string.h>

std::string remove_sticker(std::string str)
{
    bool order_flag = false;
    bool bracket_type = false;
    int start_index = 0;
    int i = 0;
    while (i < str.size())
    {
        if (order_flag)
        {
            if (bracket_type)
            {
                if ((str[i] != '(') || (i == str.size() - 1))
                {
                    order_flag = false;
                    if (i == str.size() - 1)
                    {
                        str.erase(start_index, (i - start_index) + 1);
                        break;
                    }
                    str.erase(start_index, i - start_index);
                    i = start_index;
                }
            }
            else
            {
                if ((str[i] != ')') || (i == str.size() - 1))
                {
                    order_flag = false;
                    
                    if (i == str.size() - 1)
                    {
                        str.erase(start_index, (i - start_index) + 1);
                        break;
                    }
                    str.erase(start_index, i - start_index);
                    i = start_index;
                }
            }
        }
        else
        {
            if (i < str.size() - 3)
            {
                if ((str[i] == ':') && (str[i + 1] == '-') && ((str[i + 2] == ')') || (str[i + 2] == '(')))
                {
                    if (str[i + 2] == ')')
                    {
                        bracket_type = false;
                    }
                    else
                    {
                        bracket_type = true;
                    }
                    order_flag = true;
                    start_index = i;
                    i += 2;
                }
            }
        }
        ++i;
    }
    return str;
}


int main()
{
    std::string a = "artem:-((";
    std::string res = remove_sticker(a);
    std::cout << res;
    return 0;
}