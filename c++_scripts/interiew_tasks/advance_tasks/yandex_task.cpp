//3(2ab) -> aabaabaab
//a2(a2(bc))3db -> aabcbcabcbcdddb

#include <iostream>
#include <string>


std::pair<int, int > get_index_close_bracket(const std::string & str, std::pair<int, int> indexes_global)
{
    std::pair<int, int> indexes(0, 0);
    bool flag_first_bracket = false;
    int open_bracket_counter = 0;
    for (int i = indexes_global.first + 1; i < indexes_global.second; ++i)
    {
        if (str[i] == '(')
        {
            ++open_bracket_counter;
            if (!flag_first_bracket)
            {
                indexes.first = i;
                flag_first_bracket = true;
            }
        }
        if (str[i] == ')')
        {
            indexes.second = i;
            --open_bracket_counter;
        }
        if (flag_first_bracket)
        {
            if (open_bracket_counter == 0)
            {
                return indexes;
            }
        }
    }
    return indexes;
}

std::string simlify_numerics(const std::string & str)
{
    std::string simple_str;
    for (int i = 0; i < str.size(); ++i)
    {
        if (std::isdigit(str[i]))
        {
            int shift_index = 1;
            if (i == str.size() - 1)
            {
                shift_index = -1;
            }
            int multiplier = str[i] - '0';
            for (int j = 0; j < multiplier - 1; ++j)
            {
               simple_str += str[i + shift_index];
            }
        }
        else
        {
            simple_str.push_back(str[i]);
        }
    }
    return simple_str;
}

std::string open_brackets(const std::string & str, std::pair<int, int> indexes, int multiplier)
{
    std::cout << "got str >> " << str << "\n";
    std::string modif_str = simlify_numerics(str);
    std::cout << "open_brackets modif_str >> " << modif_str << "\n";
    std::string res_str;
    if (multiplier > 1)
    {
        for (char i : modif_str)
        {
            for (int j = 0; j < multiplier - 1; ++j)
            {
                res_str += i;
            }
        }
        std::cout << "open_brackets res_str in for " << res_str << "\n";
        return res_str;
    }
    std::cout << "open_brackets res_str " << modif_str << "\n";
    return modif_str;
}

std::string simlify_str(const std::string & str, std::pair<int, int> indexes) {
    std::cout << indexes.first << " " << indexes.second << "\n";
    std::pair<int, int> bracket_indexes = get_index_close_bracket(str, indexes);
    std::cout << bracket_indexes.first << " " << bracket_indexes.second << "\n";
    if (!((bracket_indexes.first == 0) && (bracket_indexes.second == 0)))
    {
        int multiplier = 1;
        if (bracket_indexes.first != 0)
        {
            if (std::isdigit(str[bracket_indexes.first - 1]))
            {
                std::cout << "change mult\n";
                multiplier = str[bracket_indexes.first - 1] - '0';
            }
        }
        std::cout << "multiplier " << multiplier << "\n";
        std::string opened_brackets = open_brackets(simlify_str(str, bracket_indexes), bracket_indexes, multiplier);
        std::cout << "return end indexes " << indexes.first << " " << indexes.second << "\n";
        if (multiplier != 1)
        {
            return str.substr(indexes.first + 1, bracket_indexes.first - indexes.first - 2) + opened_brackets + str.substr(bracket_indexes.second + 1, indexes.second - 1 -bracket_indexes.second);
        }
        return str.substr(indexes.first + 1, bracket_indexes.first - indexes.first - 1) + opened_brackets + str.substr(bracket_indexes.second + 1, indexes.second - 1 -bracket_indexes.second);
    }
    std::cout << "just return with indexes " << indexes.first << " " << indexes.second << "\n" ;
    return str.substr(indexes.first + 1, indexes.second - indexes.first - 1);
}

std::string get_result(const std::string & str)
{
    return simlify_str(str, std::pair<int, int>(-1, str.size()));
}

int main()
{
    std::string test_1 = "qq3(2ab)a";
//    std::string test_2 = "a2(a2(bc))3db";
    std::cout << test_1 << "\n" << get_result(test_1) << "\n";
//    std::cout << test_2 << "\n" << get_result(test_2) << "\n";
    return 0;
}
