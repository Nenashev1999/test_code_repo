#include <iostream>
#include <vector>

int find_seat(const std::vector<int> & row)
{
    int max_length = 0;
    int temp_length = 0;
    int flag_first_seat = true;
    for (int i = 0; i < row.size(); ++i)
    {
        if (row[i] == 0)
        {
            ++temp_length;
            if (i == row.size() - 1)
            {
                if (max_length < temp_length)
                {
                    max_length = temp_length;
                }
            }
        }
        else
        {
            if (flag_first_seat == true)
            {
                if (max_length < temp_length)
                {
                    max_length = temp_length;
                }
                flag_first_seat = false;
            }
            else
            {
                if (temp_length % 2 == 0)
                {   
                    int temp_max = temp_length / 2;
                    if (max_length < temp_max)
                    {
                        max_length = temp_max;
                    }
                }
                else
                {
                    int temp_max = temp_length / 2 + 1;
                    if (max_length < temp_max)
                    {
                        max_length = temp_max;
                    }
                }
            }
            temp_length = 0;
        }
    }
    return max_length;
}

int main()
{
    std::vector<int> a1 = {1, 0, 0, 0, 0, 1};
    std::cout << find_seat(a1) << "\n";
    return 0;
}