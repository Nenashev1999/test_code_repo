#include <iostream>
#include <vector>

std::vector<int> find_k_closest(const std::vector<int> & a, size_t K, size_t index)
{
    int diff = 1;
    bool plus_flag = true;
    std::vector<int> res;
    if (K > 0)
    {
        res.push_back(a[index]);
    }
    else
    {
        return res;
    }

    while (res.size() < K)
    {
        if (plus_flag == true)
        {
            if (index + diff < a.size())
            {
                res.push_back(a[index + diff]);
            }
            plus_flag = false;
        }
        else
        {
            if ((int)(index - diff) > -1)
            {
                res.push_back(a[index - diff]);
            }
            ++diff;
            plus_flag = true;
        }
    }
    return res;
}


int main()
{
    auto a = find_k_closest({2, 3, 5, 7, 11, 12}, 5, 5);
    for (int i = 0; i < a.size(); ++i)
    {
        std::cout << a[i] << " ";
    }
    return 0;
}