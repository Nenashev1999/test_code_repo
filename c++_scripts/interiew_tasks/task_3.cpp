#include <iostream>
#include <vector>


struct Point{
    int time;
    int value;
};


std::vector<Point> sum_series(const std::vector<Point> & a, const std::vector<Point> & b)
{
    std::vector<Point> united;
    int index_a = 0, index_b = 0;
    while ((index_a < a.size()) || (index_b < b.size()))
    {
        if (index_a == a.size())
        {
            united.push_back(b[index_b]);
            ++index_b;
        }
        else if (index_b == b.size())
        {
            united.push_back(a[index_a]);
            ++index_a;
        }
        else if (a[index_a].time < b[index_b].time)
        {
            united.push_back(a[index_a]);
            ++index_a;
        }
        else
        {
            united.push_back(b[index_b]);
            ++index_b;
        }
    }
    for (int i = 1; i < united.size(); ++i)
    {
        united[i].value += united[i - 1].value;
    }
    return united;
}


int main()
{   
    auto a = sum_series({{1, 1}, {3, 3}}, {{2, 2}, {4, 4}});
    for (auto & k : a)
    {
        std::cout << k.time << " " << k.value << "; ";
    }
    std::cout << "\n";
    return 0;
}