#include <iostream>
#include <math.h>

struct Sides
{
    Sides() : side_1(1), side_2(1), side_3(1), side_4(1) {}
    Sides(int a_1, int a_2, int a_3, int a_4) : side_1(a_1), side_2(a_2), side_3(a_3), side_4(a_4) {}
    Sides(int a_1, int a_2) : side_1(a_1), side_2(a_2), side_3(a_1), side_4(a_2) {}
    Sides(int a) : side_1(a), side_2(a), side_3(a), side_4(a) {}

    int side_1;
    int side_2;
    int side_3;
    int side_4;

    int get_sum()
    {
        return side_1 + side_2 + side_3 + side_4;
    }

    int get_mult()
    {
        return side_1 * side_2 * side_3 * side_4;
    }
};

struct Angles
{
    Angles() : angle_1(90), angle_2(90), angle_3(90), angle_4(90) {}
    Angles(int a_1, int a_2, int a_3, int a_4) : angle_1(a_1), angle_2(a_2), angle_3(a_3), angle_4(a_4) {}
    Angles(int a_1, int a_2) : angle_1(a_1), angle_2(a_2), angle_3(a_1), angle_4(a_2) {}
    Angles(int a) : angle_1(a), angle_2(a), angle_3(a), angle_4(a) {}
    int angle_1;
    int angle_2;
    int angle_3;
    int angle_4;
};

class Qadrilateral
{
public:
    Sides sides;
    Angles angles;
    Qadrilateral() {}
    Qadrilateral(int a_1, int a_2, int a_3, int a_4, int b_1, int b_2, int b_3, int b_4) : sides(a_1, a_2, a_3, a_4), angles(b_1, b_2, b_3, b_4) {}
    double get_square()
    {
        std::cout << "get square Qadrilateral" << "\n";
        double p = sides.get_sum() / 2;
        return sqrt((p - sides.side_1) * (p - sides.side_2) * (p - sides.side_3) * 
               (p - sides.side_4) - sides.get_mult() * pow(cos((deg2rad(angles.angle_1) + deg2rad(angles.angle_3)) / 2), 2));
    }
    
    double deg2rad(int angle)
    {
        return angle * 3.14 / 180;
    }

    double get_perimeter()
    {
        return sides.get_sum();
    }
};


class Rectangle: public Qadrilateral
{
public:
    Rectangle(){}
    Rectangle(int a_1, int a_2) : Qadrilateral(a_1, a_2, a_1, a_2, 90, 90, 90, 90) {}

    double get_square()
    {
        std::cout << "get square Rectangle" << "\n";
        return sides.side_1 * sides.side_2;
    }
};

class Rhombus : public Qadrilateral
{
public:
    Rhombus(int a, int b_1, int b_2) : Qadrilateral(a, a, a, a, b_1, b_2, b_1, b_2) {}
};

class Square : public Rectangle
{
public:
    Square(){}
    Square(int a) : Rectangle(a, a) {}

    double get_square()
    {
        std::cout << "get square Square" << "\n";
        return sides.side_1 * sides.side_1;
    }
};