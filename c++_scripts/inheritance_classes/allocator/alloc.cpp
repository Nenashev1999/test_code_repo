#include <iostream>

template <class T>
class Alloc
{
public:
    typedef T value_type;
    template <class U>
    Alloc(const Alloc<U> &){}
    T* allocate(size_t n) const
    {
        return (T*) (new char[n * sizeof(T)]);
    }

    void deallocate(T* p, size_t n) const
    {
        delete [] (char) p;
    }

    template <typename... Args>
    void* construct(T* p, const Args & ... args) const
    {
        return new (p) T(args...);
    }

    void destroy(T* p)
    {
        p->~T();
    }
};

int main()
{
    return 0;
}